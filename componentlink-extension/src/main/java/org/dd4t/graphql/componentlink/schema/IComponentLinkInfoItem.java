/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.dd4t.graphql.componentlink.schema;
import org.dd4t.graphql.componentlink.resolver.ComponentLinkInfoResolver;
import com.sdl.delivery.content.model.core.schema.Node;

import graphql.annotations.annotationTypes.GraphQLDataFetcher;
import graphql.annotations.annotationTypes.GraphQLDescription;
import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLPrettify;
import graphql.annotations.annotationTypes.GraphQLTypeResolver;
import graphql.schema.PropertyDataFetcher;

/**
 * The interface specifying the mapping from the fetcher's data to the GraphQL response.
 * This effectively defines the schema of the type used in the service.
 *
 */
@GraphQLDescription("ComponentLinkInfo Item")
@GraphQLTypeResolver(ComponentLinkInfoResolver.class)
public interface IComponentLinkInfoItem extends Node {
    @GraphQLField
    @GraphQLPrettify
    @GraphQLDescription("Publication ID of the link")
    @GraphQLDataFetcher(value = PropertyDataFetcher.class, args = "publicationId")
    default String getPublicationId() {
        return null;
    }

    @GraphQLField
    @GraphQLPrettify
    @GraphQLDescription("Component ID of the link")
    @GraphQLDataFetcher(value = PropertyDataFetcher.class, args = "componentId")
    default String getComponentId() {
        return null;
    }

    @GraphQLField
    @GraphQLPrettify
    @GraphQLDescription("Page ID of the link")
    @GraphQLDataFetcher(value = PropertyDataFetcher.class, args = "pageId")
    default String getPageId() {
        return null;
    }

    @GraphQLField
    @GraphQLPrettify
    @GraphQLDescription("Template ID of the link")
    @GraphQLDataFetcher(value = PropertyDataFetcher.class, args = "templateId")
    default String getTemplateId() {
        return null;
    }
    
    @GraphQLField
    @GraphQLPrettify
    @GraphQLDescription("Template priority of the link")
    @GraphQLDataFetcher(value = PropertyDataFetcher.class, args = "priority")
    default String getPriority() {
        return null;
    }

    @GraphQLField
    @GraphQLPrettify
    @GraphQLDescription("Resolved URL (if any)")
    @GraphQLDataFetcher(value = PropertyDataFetcher.class, args = "url")
    default String getUrl() {
        return null;
    }
}
