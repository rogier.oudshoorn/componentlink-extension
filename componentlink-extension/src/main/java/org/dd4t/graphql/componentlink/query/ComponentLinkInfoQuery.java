/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.dd4t.graphql.componentlink.query;

import static com.sdl.delivery.content.utils.ContentAPIConstants.CM_URI;

import java.util.ArrayList;
import java.util.List;

import org.dd4t.graphql.componentlink.data.ComponentLinkInfo;
import org.dd4t.graphql.componentlink.datafetcher.ComponentLinkInfoFetcher;
import org.dd4t.graphql.componentlink.schema.IComponentLinkInfoItem;
import com.sdl.delivery.content.model.core.query.ContentQuery;

import graphql.annotations.annotationTypes.GraphQLDataFetcher;
import graphql.annotations.annotationTypes.GraphQLDescription;
import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLName;
import graphql.annotations.annotationTypes.GraphQLNonNull;
import graphql.annotations.annotationTypes.GraphQLPrettify;
import graphql.annotations.annotationTypes.GraphQLTypeExtension;

/**
 * The root level query for getting {@link ComponentLinkInfo}.
 * Actually this class can implement multiple queries, but we may go with a pattern of only one per query class.
 *
 */
@GraphQLTypeExtension(ContentQuery.class)
public class ComponentLinkInfoQuery {
    @GraphQLField
    @GraphQLPrettify
    @GraphQLDescription("Gets component links")
    @GraphQLDataFetcher(ComponentLinkInfoFetcher.class)
    public List<IComponentLinkInfoItem> getComponentLinks(@GraphQLNonNull @GraphQLName(CM_URI) @GraphQLDescription("CM URI") String cmUri) {
        return new ArrayList<>();
    }

}
