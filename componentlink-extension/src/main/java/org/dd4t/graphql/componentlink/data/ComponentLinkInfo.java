/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.dd4t.graphql.componentlink.data;

import org.dd4t.graphql.componentlink.datafetcher.ComponentLinkInfoFetcher;
import com.sdl.delivery.content.data.ContextDataAware;
import com.sdl.delivery.content.model.core.schema.Node;
import com.tridion.storage.LinkInfo;

/**
 * The model class returned from the {@link ComponentLinkInfoFetcher}.
 * This models the parts of the {@link LinkInfo} we find potential relevant for the service.
 *
 */
public class ComponentLinkInfo extends ContextDataAware {
	private static final long serialVersionUID = 1L;
    private int publicationId;
    private int componentId;
    private int pageId;
    private int templateId;
    private int priority;
    private String url;

    public ComponentLinkInfo(int publicationId, int componentId, int pageId, int templateId, int priority, String url) {
    	this.publicationId = publicationId;
    	this.componentId = componentId;
        this.pageId = pageId;
        this.templateId = templateId;
        this.priority = priority;
        this.url = url;
    }

    /**
     * The id mandated by the Tridion {@link Node} interface.
     * In this case it's pretty arbitrary and we feel no need to rename it.
     * 
     * @return
     */
    public int getId() {
        return 1;
    }

    public int getPageId() {
        return pageId;
    }
    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    public int getPriority() {
        return priority;
    }
    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

	public int getPublicationId() {
		return publicationId;
	}
	public void setPublicationId(int publicationId) {
		this.publicationId = publicationId;
	}

	public int getComponentId() {
		return componentId;
	}
	public void setComponentId(int componentId) {
		this.componentId = componentId;
	}

	public int getTemplateId() {
		return templateId;
	}
	public void setTemplateId(int templateId) {
		this.templateId = templateId;
	}
    
}
