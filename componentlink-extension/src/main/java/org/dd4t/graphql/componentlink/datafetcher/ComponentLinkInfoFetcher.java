/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.dd4t.graphql.componentlink.datafetcher;

import static com.sdl.delivery.content.utils.ContentAPIConstants.CM_URI;

import java.util.LinkedList;
import java.util.List;

import org.dd4t.graphql.componentlink.data.ComponentLinkInfo;
import com.sdl.delivery.content.model.core.datafetcher.AbstractListDataFetcher;
import com.sdl.delivery.content.model.core.datafetcher.ExecutionArguments;
import com.tridion.storage.LinkInfo;
import com.tridion.storage.StorageManagerFactory;
import com.tridion.storage.StorageTypeMapping;
import com.tridion.storage.dao.LinkInfoDAO;
import com.tridion.util.TCMURI;

import graphql.schema.DataFetchingEnvironment;

/**
 * This {@link AbstractListDataFetcher} fetches all target {@link LinkInfo}s for the provided component id.
 *
 */
public class ComponentLinkInfoFetcher extends AbstractListDataFetcher<List<ComponentLinkInfo>, ComponentLinkInfo> {

    @Override
    protected List<ComponentLinkInfo> fetch(DataFetchingEnvironment dataFetchingEnvironment, ExecutionArguments executionArguments) {
        List<ComponentLinkInfo> list = new LinkedList<>();

        try {
        	TCMURI tcmUri = new TCMURI(executionArguments.getArgument(CM_URI));
            LinkInfoDAO linkInfoDAO = (LinkInfoDAO) StorageManagerFactory.getDAO(tcmUri.getPublicationId(), StorageTypeMapping.LINK_INFO);
            List<LinkInfo> clinks = linkInfoDAO.findByComponent(tcmUri.getPublicationId(), tcmUri.getItemId());

            for (LinkInfo link : clinks) {
                list.add(new ComponentLinkInfo(link.getPublicationId(), link.getComponentId(), link.getPageId(), link.getTemplateId(), link.getTemplatePriority(), link.getUrl()));
            }
        } catch (Exception ex) {
        	/*
        	 * We should find out how to do proper error handling.
        	 * This is OK for the purpose of this fetcher.
        	 */
        	throw new RuntimeException(ex);
        }

        return list;
    }

    @Override
    protected List<ComponentLinkInfo> wrap(List<ComponentLinkInfo> list, ExecutionArguments executionArguments) {
        return list;
    }

    @Override
    public Class<ComponentLinkInfo> getModelClass() {
        return ComponentLinkInfo.class;
    }

    @Override
    protected boolean isConnection() {
        return false;
    }
}
